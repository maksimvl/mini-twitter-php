<?php
include ('db.php');
session_start();
$_SESSION['user_id'];
$params = $_POST;

$sql = "SELECT * FROM t195965_users WHERE id = '" . $_SESSION['user_id'] . "'";

$result = mysqli_query($conn, $sql);

$user = mysqli_fetch_array($result);
?>

<?php
if ($_SESSION['user_id'] > 0) {
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.php">Twitter</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
                  <form action="find.php" method="POST">
  	                <div class="col-lg-12">

  	                  <div class="input-group form">

  	                       <input type="text" name="find" class="form-control" placeholder="Search...">
  	                       <span class="input-group-btn">
                             <input class="btn btn-primary" value="Search" type="submit">
  	                       </span>
  	                  </div>
  	                </div>

  	              </div>

                </form>
	           </div>

	           <div class="col-md-2">

	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.php">Profile</a></li>
	                          <li><a href="logout.php">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

  <div class="page-content">

<div class="row">
<div class="col-md-2">
  <div class="sidebar content-box" style="display: block;">
          <ul class="nav">
              <!-- Main menu -->
              <!-- <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> New post</a></li>
              <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
              <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
              <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
              <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li> -->
              <li><a href="index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
              <li><a href="post.php"><i class="glyphicon glyphicon-pencil"></i> New Post</a></li>
              <li><a href="myfollowers.php"><i class="glyphicon glyphicon-tasks"></i>Edit followers</a></li>
       </div>

</div>
		  <div class="col-md-10">
		  	<div class="row">
          <div class="content-box-large box-with-header">
            <div class="container">
            <div class="row">
            <div class="col-md-10 ">
            <form action="save.php" class="form-horizontal" role="form" method="POST">
            <fieldset>

            <!-- Form Name -->
            <legend>User profile</legend>
            <div class="form-group">
              <?php if(isset($_SESSION['window_message']) && ! empty($_SESSION['window_message'])) { ?>
                  <div class="col-md-8 col-sm-6 col-xs-12">
                      <div class="alert alert-info alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <ul>
                              <?php foreach($_SESSION['window_message'] as $mesasge) { ?>
                              <li><?php echo $mesasge; unset($_SESSION['window_message']) ?></li>
                              <?php } ?>
                          </ul>
                      </div>
                  </div>
              <?php } ?>

            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Name (Full name)">Username</label>
              <div class="col-md-4">
             <div class="input-group">
                   <div class="input-group-addon">
                    <i class="fa fa-user">
                    </i>
                   </div>
                   <span class="form-control input-md"><?php echo htmlentities($user['username']) ?> </span>
                  </div>
              </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Email Address">Email Address</label>
              <div class="col-md-4">
              <div class="input-group">
                   <div class="input-group-addon">
                 <i class="fa fa-envelope-o"></i>
                   </div>
                <input id="email" name="user_email" type="text" placeholder="email" class="form-control input-md" value="<?php echo htmlentities($user['email']) ?>">
                  </div>

              </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Date Of Birth">Firstname</label>
              <div class="col-md-4">
              <div class="input-group">
                   <div class="input-group-addon">
                 <i class="fa fa-birthday-cake"></i>
                   </div>
                   <input id="Firstname" name="first_name" type="text" placeholder="Firstname" class="form-control input-md" value="<?php echo htmlentities($user['firstname']) ?>">
                  </div>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Father">Lastname</label>
              <div class="col-md-4">
              <div class="input-group">
                   <div class="input-group-addon">
                  <i class="fa fa-male" style="font-size: 20px;"></i>
                   </div>
                  <input id="lastname" name="last_name" type="text" placeholder="lastname" class="form-control input-md" value="<?php echo htmlentities($user['lastname']) ?>">
                  </div>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Father">New password</label>
              <div class="col-md-4">
              <div class="input-group">
                   <div class="input-group-addon">
                  <i class="fa fa-male" style="font-size: 20px;"></i>
                   </div>
                  <input id="lastname" name="user_password" type="text" placeholder="New password" class="form-control input-md">
                  </div>
              </div>
            </div>

            <!-- Multiple Radios (inline) -->
            <!-- <div class="form-group">
              <label class="col-md-4 control-label" for="Gender">Gender</label>
              <div class="col-md-4">
                <label class="radio-inline" for="Gender-0">
                  <input type="radio" name="Gender" id="Gender-0" value="1" checked="checked">
                  Male
                </label>
                <label class="radio-inline" for="Gender-1">
                  <input type="radio" name="Gender" id="Gender-1" value="2">
                  Female
                </label>
              </div>
            </div> -->

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Overview (max 200 words)">Overview (max 200 words)</label>
              <div class="col-md-4">
                <textarea class="form-control" rows="10"  id="story" name="story" ><?php echo htmlentities($user['story']) ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" ></label>
              <div class="col-md-4">
              <input class="btn btn-primary" value="Save Changes" type="submit">
              <a href="index.php" class="btn btn-primary"> Twitter </a>
              </div>
            </div>
            </form>
            <div class="form-group">
              <label class="col-md-4 control-label" for="Upload photo">Upload photo</label>
              <div class="col-md-4">
                <form action="upload.php" method="post" enctype="multipart/form-data">
                <input class="text-center center-block well well-sm" type="file" name="fileToUpload" id="fileToUpload ">
                <input class="btn btn-default" type="submit" value="Upload Image" name="submit">
            </form>
              </div>
            </div>
            </fieldset>

            </div>
            <div class="col-md-2 hidden-xs">
            <img src="<?php echo $user['photo']; ?>" class="img-responsive img-thumbnail ">
              </div>
            </div>
            </div>

      <footer>
         <div class="container">

            <div class="copy text-center">
                <a href='#'>Maksim Ljaussov</a>
            </div>

         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>
<?php
} else {
header("location:login.php");
}
?>
