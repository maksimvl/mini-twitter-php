<?php

include ('db.php');
session_start();
$_SESSION['user_id'];
$save = $_POST;
$firstname = mysqli_real_escape_string($conn, $save['first_name']);
$lastname = mysqli_real_escape_string($conn, $save['last_name']);
$email = mysqli_real_escape_string($conn, $save['user_email']);
$story = mysqli_real_escape_string($conn, $save['story']);

$update = "UPDATE t195965_users SET firstname = '" . $firstname . "', lastname = '" . $lastname . "', email = '" . $email . "', story = '" . $story . "'  WHERE id = '" . $_SESSION['user_id'] . "'";

if (mysqli_query($conn, $update)) {
    $_SESSION['window_message'][] = "Record updated successfully";
    header("location:profile.php");
} else {
    echo "Error updating record: " . mysqli_error($conn);
}

if (!empty($save['user_password'])) {
    $password = mysqli_real_escape_string($conn, $save['user_password']);
    $passwordHash = hash('sha256', $password);


    $update = "UPDATE t195965_users SET password = '" . $passwordHash . "'  WHERE id = '" . $_SESSION['user_id'] . "'";
    if (mysqli_query($conn, $update)) {
        $_SESSION['window_message'][] = "Password changed";
        header("location:profile.php");
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>
