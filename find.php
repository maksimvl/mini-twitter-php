<?php

include 'db.php';
session_start();
$_SESSION['user_id'];
$find = $_POST;
$findValue = mysqli_real_escape_string($conn, $find['find']);
$findSql = "SELECT * FROM `t195965_users` WHERE username LIKE '".$findValue."%' AND id != '".$_SESSION['user_id']."'";
$result = mysqli_query($conn, $findSql);
$user = mysqli_fetch_array($result);
// die(var_dump($result));
if (!empty($find)) {
    if (empty($find['find'])) {
        $_SESSION['window_message'][] = "Input user name!!!";
        header("location:index.php");
    }
    if (count($user) === 0) {
        $_SESSION['window_message'][] = "User not found";
        header("location:index.php");
    }
}

?>

<?php
if ($_SESSION['user_id'] > 0) {
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.php">Twitter</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
                  <form action="find.php" method="POST">
  	                <div class="col-lg-12">

  	                  <div class="input-group form">

  	                       <input type="text" name="find" class="form-control" placeholder="Search...">
  	                       <span class="input-group-btn">
                             <input class="btn btn-primary" value="Search" type="submit">
  	                       </span>
  	                  </div>
  	                </div>

  	              </div>

                </form>
	           </div>

	           <div class="col-md-2">

	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.php">Profile</a></li>
	                          <li><a href="logout.php">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">

    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <!-- <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> New post</a></li>
                    <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li> -->
                    <li><a href="index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                    <li><a href="post.php"><i class="glyphicon glyphicon-pencil"></i> New Post</a></li>
                    <li><a href="myfollowers.php"><i class="glyphicon glyphicon-tasks"></i>Edit followers</a></li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
		  	<div class="row">
          <div class="content-box-large box-with-header">
          <table class="table table-condensed">
             <thead>
               <tr>
                 <th>Username</th>
                 <th>Fristname</th>
                 <th>Lastname</th>
                 <th>Button</th>
               </tr>
             </thead>
             <tbody>
               <?php
               foreach ($result as $r) {

               ?>
               <tr>
                 <td><?php echo htmlentities($r['username']) ?></td>
                 <td><?php echo htmlentities($r['firstname']) ?></td>
                 <td><?php echo htmlentities($r['lastname']) ?></td>
                 <td><a href="userprofile.php?userId=<?php echo htmlentities($r['id'])?> " class="btn btn-info" role="button">See profile</a></td>
               </tr>
               <?php
                  }
               ?>
             </tbody>
          </table>
        </div>
		  	</div>
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">

            <div class="copy text-center">
               <a href='#'>Maksim Ljaussov</a>
            </div>

         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>
<?php
} else {
header("location:login.php");
}
?>
