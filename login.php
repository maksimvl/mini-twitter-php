<?php
include 'db.php';
$messages = array();
$params = $_POST;

$logged = false;

if (!empty($params)) {
    if (empty($params['user_name'])) {
        array_push($messages, 'input user name');
    }

    if (empty($params['user_password'])) {
        array_push($messages, 'input password');
    }

    if (!empty($params['user_name']) && !empty($params['user_password'])) {
        $password_hash = hash('sha256', $params['user_password']);
        $sql = ("SELECT username, id, gender, password FROM t195965_users WHERE username = '".$params['user_name']."' AND  password = '".$password_hash."'");
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($result);
        if (count($row) > 0) {
            session_start();
            $_SESSION['user_id'] = $row['id'];
            $_SESSION['user_gender'] = $row['gender'];
            $logged = true;
            header('Refresh: 0; url=profile.php');
            if (mysqli_query($conn, $sql)) {
            } else {
                echo 'Error: '.$sql.'<br>'.mysqli_error($conn);
            }
        } else {
            array_push($messages, 'The username or password are incorrect!');
        }
    }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1 style="color: #FFFAFA; text-align:center">Twitter</h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Sign In</h6>
                      <form class="form-signin" method="POST">
                          <?php
                          if (!empty($messages)) {
                              echo '<div class="alert alert-danger" role="alert">';
                              echo '<ul>';
                              foreach ($messages as $message) {
                                  echo '<li>'.$message.'</li>';
                              }
                              echo '</ul>';
                              echo '</div>';
                          }
                          ?>

                          <?php
                          if ($logged) {
                              echo '<div class="alert alert-success" role="alert">';
                              echo 'Successfully! ';
                              echo ' Please wait.';
                              echo '</div>';
                          }
                          ?>
                          <input type="text" class="form-control" name="user_name" placeholder="Username"  autofocus="" />
                          <input type="password" class="form-control" name="user_password" placeholder="Password" />
                          <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                      </form>
			            </div>
			        </div>

			        <div class="already">
			            <p>Don't have an account yet?</p>
			            <a href="registration.php">Sign Up</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>
