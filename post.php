<?php
include ('db.php');
session_start();
$_SESSION['user_id'];
$messages = array();
$registerPost = false;
$params = $_POST;
$now = date("Y-m-d H:i:s");

if (!empty($params)) {

    if (empty($params['title'])) {

        array_push($messages, "input title");
    }

    if (empty($params['post'])) {

        array_push($messages, "input post");
    }

    if (!empty($params['title']) && !empty($params['post'])){
      $post = mysqli_real_escape_string($conn, $params['post']);
      $title = mysqli_real_escape_string($conn, $params['title']);
      $sql = "INSERT INTO t195965_posts (user_id, titile, text, date) VALUES ('" . $_SESSION['user_id'] . "', '" . $title . "', '" . $post . "', '".$now."')";
      if (mysqli_query($conn, $sql)) {
          $registerPost = true;
          header("location:index.php");
      } else {
          echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
    }
    mysqli_close($conn);
}
?>

<?php
if ($_SESSION['user_id'] > 0) {
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
    <style>
      #disabled {
        pointer-events: none;
        cursor: default;
      }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.php">Twitter</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
                  <form action="find.php" method="POST">
  	                <div class="col-lg-12">

  	                  <div class="input-group form">

  	                       <input type="text" name="find" class="form-control" placeholder="Search...">
  	                       <span class="input-group-btn">
                             <input class="btn btn-primary" value="Search" type="submit">
  	                       </span>
  	                  </div>
  	                </div>

  	              </div>

                </form>
	           </div>

	           <div class="col-md-2">

	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.php">Profile</a></li>
	                          <li><a href="logout.php">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

  <div class="page-content">

<div class="row">
<div class="col-md-2">
  <div class="sidebar content-box" style="display: block;">
          <ul class="nav">
              <!-- Main menu -->
              <!-- <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> New post</a></li>
              <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
              <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
              <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
              <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li> -->
              <li><a href="index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
              <li><a href="post.php"><i class="glyphicon glyphicon-pencil"></i> New Post</a></li>
              <li><a href="myfollowers.php"><i class="glyphicon glyphicon-tasks"></i>Edit followers</a></li>
       </div>

</div>
		  <div class="col-md-10">
		  	<div class="row">
          <div class="content-box-large box-with-header">
            <div class="container">
            <div class="row">
            <div class="col-md-10 ">
            <div class="form-horizontal">
            <fieldset>
              <?php
              if (!empty($messages)) {
                echo '<div class="alert alert-danger" role="alert">';
                echo '<ul>';
                foreach ($messages as $message) {
                  echo "<li>" . $message . "</li>";
                }
                echo '</ul>';
                echo '</div>';
              }
              ?>
              <form class="form-horizontal" role="form" method="post">
              	<div class="form-group">
              		<label for="title" class="col-sm-2 control-label">Title</label>
              		<div class="col-sm-10">
              			<input type="title" class="form-control" id="title" name="title" placeholder="Title">
              		</div>
              	</div>
              	<div class="form-group">
              		<label for="post" class="col-sm-2 control-label">Twitt</label>
              		<div class="col-sm-10">
              			<textarea class="form-control" rows="4" name="post"></textarea>
              		</div>
              	</div>
              	<div class="form-group">
              		<div class="col-sm-10 col-sm-offset-2">
              			<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
                    <a href="index.php" class="btn btn-primary">Back to home </a>
              		</div>
              	</div>
              	<div class="form-group">
              		<div class="col-sm-10 col-sm-offset-2">
              		</div>
              	</div>
              </form>

            </fieldset>

            </div>
            </div>
            </div>
        </div>
		  	</div>
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">

            <div class="copy text-center">
                <a href='#'>Maksim Ljaussov</a>
            </div>

         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>
<?php
} else {
header("location:login.php");
}
?>
