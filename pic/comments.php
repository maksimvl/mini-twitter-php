<?php
ob_start();
include ('db.php');
session_start();
$_SESSION['user_id'];
$messages = array();
$registerPost = false;
$params = $_POST;
$now = date("Y-m-d H:i:s");


if(isset($_GET['id']) && $_GET['id'] !== ''){
    $post_id = $_GET['id'];
    
  } else {
    echo "failed";
  }
  $user = "SELECT * FROM users WHERE id = '" . $_SESSION['user_id'] . "'";
  
  $resultUser = mysqli_query($conn, $user);

  $userName = mysqli_fetch_array($resultUser);

  $sql = "SELECT * FROM posts WHERE id = $post_id";

  $result = mysqli_query($conn, $sql);

  $post = "SELECT * FROM comments WHERE post_id = $post_id";

  $resultComments = mysqli_query($conn, $post);

    if (isset($_POST['post'])) {
    if (empty($params['post'])) {

        array_push($messages, "input post");
    }

    if (!empty($params['post'])){
      $post = mysqli_real_escape_string($conn, $params['post']);
      
      $sql2 = "INSERT INTO comments (post_id, user_id, username, date, text) VALUES ('" . $post_id . "','" . $userName['id'] . "', '" . $userName['username'] . "', '".$now."', '" . $post . "' )";
      if (mysqli_query($conn, $sql2)) {
          $registerPost = true;
          header("Location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
          exit;
      } else {
          echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
      }
    }
    mysqli_close($conn);
    exit();
    }



  ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    
    <link href="css/likes.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 
  <h1 style="text-align:center">Post</h1>

<div class="container">
<?php foreach ($result as $r) {?>
  <div class="panel panel-default">
    <div class="panel-heading"><?php echo htmlentities($r['titile']) ?></div>
    <div class="panel-body"><?php echo htmlentities($r['text']) ?></div>
  </div>
</div>
<?php }?>

<h1 style="text-align:center">Comments</h1>

<div class="container">
<?php foreach ($resultComments as $r) {?>
  <div class="panel panel-default">
    <div class="panel-heading"><?php echo htmlentities($r['username']) ?>, <?php echo htmlentities($r['date']) ?></div>
    <div class="panel-body"><?php echo htmlentities($r['text']) ?></div>
  </div>
</div>
<?php }?>

<div class="col-md-10">
		  	<div class="row">
          <div class="content-box-large box-with-header">
            <div class="container">
            <div class="row">
            <div class="col-md-10 ">
            <div class="form-horizontal">
            <fieldset>
              <?php
              if (!empty($messages)) {
                echo '<div class="alert alert-danger" role="alert">';
                echo '<ul>';
                foreach ($messages as $message) {
                  echo "<li>" . $message . "</li>";
                }
                echo '</ul>';
                echo '</div>';
              }
              ?>
              <form class="form-horizontal" role="form" method="post">
              	</div>
              	<div class="form-group">
              		<label for="post" class="col-sm-2 control-label">Your comment</label>
              		<div class="col-sm-10">
              			<textarea class="form-control" rows="4" name="post"></textarea>
              		</div>
              	</div>
              	<div class="form-group">
              		<div class="col-sm-10 col-sm-offset-2">
              			<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
                    <a href="index.php" class="btn btn-primary">back </a>
              		</div>
              	</div>
              	<div class="form-group">
              		<div class="col-sm-10 col-sm-offset-2">
              		</div>
              	</div>
              </form>

            </fieldset>

            </div>
            </div>
            </div>
        </div>
		  	</div>
		  </div>
		</div>
    </div>



  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/likes.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/1507224fb4.js" crossorigin="anonymous"></script>
    <script>
    </script>
  </body>
</html>
