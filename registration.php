<?php
include ('db.php');
$params = $_POST;
$messages = array();
$register = false;
if (!empty($params)) {

    if (empty($params['user_name'])) {

        array_push($messages, "input user name");
    }

    if (empty($params['user_email'])) {

        array_push($messages, "input email");
    }


    if (empty($params['first_name'])) {

        array_push($messages, "input firstname");
    }

    if (empty($params['last_name'])) {

        array_push($messages, "input lastname");
    }

    if (empty($params['user_password'])) {

        array_push($messages, "input password");
    }

    if (!empty($params['user_name']) && !empty($params['user_password']) && !empty($params['user_email']) && !empty($params['first_name']) && !empty($params['last_name'])) {
        $sql = "SELECT * FROM t195965_users WHERE username = '" . $params['user_name'] . "'";

        $result = mysqli_query($conn, $sql);

        $row = mysqli_fetch_array($result);

        if (count($row) === 0) {
            $username = mysqli_real_escape_string($conn, $params['user_name']);
            $password = mysqli_real_escape_string($conn, $params['user_password']);
            $email = mysqli_real_escape_string($conn, $params['user_email']);
            $firstname = mysqli_real_escape_string($conn, $params['first_name']);
            $lastname = mysqli_real_escape_string($conn, $params['last_name']);
            $sex = mysqli_real_escape_string($conn, $params['sex']);
            $password_hash = hash('sha256', $password);
            $sql = "INSERT INTO t195965_users (username, password, email, firstname, lastname, gender) VALUES ('" . $username . "', '" . $password_hash . "', '" . $email . "', '" . $firstname . "', '" . $lastname . "', '" . $sex . "')";
            if (mysqli_query($conn, $sql)) {
                $register = true;
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        } else {
            array_push($messages, "username already exists");
        }
    }
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1>Twitter</h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <h6>Sign Up</h6>
                      <?php
                      if (!empty($messages)) {
                        echo '<div class="alert alert-danger" role="alert">';
                        echo '<ul>';
                        foreach ($messages as $message) {
                          echo "<li>" . $message . "</li>";
                        }
                        echo '</ul>';
                        echo '</div>';
                      }
                      ?>

                      <?php
                      if ($register) {
                        echo '<div class="alert alert-success" role="alert">';
                        echo "Successfully! ";
                        echo "Now you can <a href='login.php' >sign in</a>";
                        echo '</div>';
                      }
                      ?>
                      <form role="form" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="user_name" id="first_name" class="form-control input-sm" placeholder="Usename" value="<?php echo isset($_POST['user_name']) ? $_POST['user_name'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" name="user_email" id="email" class="form-control input-sm" placeholder="Email Address" value ="<?php echo isset($_POST['user_email']) ? $_POST['user_email'] : '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="first_name" id="firstname" class="form-control input-sm" placeholder="Firstname" value ="<?php echo isset($_POST['first_name']) ? $_POST['first_name'] : '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="last_name" id="lastname" class="form-control input-sm" placeholder="Lastname" value ="<?php echo isset($_POST['last_name']) ? $_POST['last_name'] : '' ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" name="user_password" id="password" class="form-control input-sm" placeholder="Password" value ="<?php echo isset($_POST['user_password']) ? $_POST['user_password'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <form><div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="radio" name="sex" value="M" checked> Male
                                    <br>
                                    <input type="radio" name="sex" value="F"> Female
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="Register" class="btn btn-primary btn-block">
                    </form>
			            </div>
			        </div>

			        <div class="already">
			            <p>Have an account already?</p>
			            <a href="login.php">Login</a>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>
